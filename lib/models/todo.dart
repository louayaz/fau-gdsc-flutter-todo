class ToDo {
  String? id;
  String? todoText;
  bool isDone;

  ToDo({
    required this.id,
    required this.todoText,
    this.isDone = false,
  });

  static List<ToDo> dummyTodoList() {
    // var strList = ['str', ''];
    // var year
    // for (final obj in strList) {
    //   print(obj);
    // }

    // for (int month = 1; month <= 12; month++) {
    //   print(month);
    // }

    // while (year < 2016) {
    //   year += 1;
    // }

    return [
      ToDo(id: '01', todoText: 'Morning Excercise', isDone: true),
      ToDo(id: '02', todoText: 'Buy Groceries', isDone: true),
      ToDo(
        id: '03',
        todoText: 'Check Emails',
      ),
      ToDo(
        id: '04',
        todoText: 'Team Meeting',
      ),
      ToDo(
        id: '05',
        todoText: 'Work on mobile apps for 2 hour',
      ),
      ToDo(
        id: '06',
        todoText: 'Dinner with Jenny',
      ),
      ToDo(
        id: '07',
        todoText: 'Dinner with Jenny',
      ),
      ToDo(
        id: '08',
        todoText: 'Dinner with Jenny',
      ),
      ToDo(
        id: '09',
        todoText: 'Dinner with Jenny',
      ),
    ];
  }
}
