import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:flutter/material.dart';
import 'package:testapp/models/todo.dart';
import 'package:testapp/widgets/todo_card.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List<ToDo> todolist = [];
  final _todoController = TextEditingController(text: '');

  @override
  void initState() {
    todolist = ToDo.dummyTodoList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      backgroundColor: Color.fromARGB(255, 245, 245, 245),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: ListView(
              children: todolist.map((todo) {
                return TodoCard(
                    todo: todo,
                    onToDoChanged: (ToDo todoItem) {
                      setState(() {
                        todoItem.isDone = !todoItem.isDone;
                      });
                    },
                    onDeleteItem: (id) {
                      setState(() {
                        todolist.removeWhere((item) => item.id == id);
                      });
                    });
              }).toList(),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.only(
                bottom: 20,
                right: 20,
                left: 20,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(right: 10),
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 2,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.0, 0.0),
                            blurRadius: 10.0,
                            spreadRadius: 0.0,
                          ),
                        ],
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextField(
                        controller: _todoController,
                        decoration: InputDecoration(
                            hintText: 'Add a new todo item',
                            border: InputBorder.none),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.blueAccent,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.0, 0.0),
                          blurRadius: 10.0,
                          spreadRadius: 0.0,
                        ),
                      ],
                    ),
                    child: IconButton(
                      padding: EdgeInsets.all(15),
                      color: Colors.white,
                      iconSize: 18,
                      icon: Icon(Icons.add),
                      onPressed: () {
                        if (_todoController.text != '') {
                          todolist.add(ToDo(
                              id: (todolist.length + 1).toString(),
                              todoText: _todoController.text));
                          setState(() {
                            _todoController.text = '';
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: const Color.fromARGB(255, 245, 245, 245),
      elevation: 0,
      title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.menu,
            color: Colors.black,
            size: 30,
          ),
        ),
        Text(
          'My Todo List',
          style: TextStyle(color: Colors.black),
        ),
      ]),
    );
  }
}

// // string
// var name = "Voyager";

// // int
// var year = 1997;

// // floar
// var diameter = 3.7;

// // list
// var strList = ['Jupiter', 'Saturn'];

// // dictionary
// var imgs = {
//   'tags': ['saturn'],
//   'url': '//path/to/satrun.jpg'
// }
